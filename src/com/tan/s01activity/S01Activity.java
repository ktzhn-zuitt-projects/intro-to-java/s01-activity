package com.tan.s01activity;

public class S01Activity {
    public static void main(String[] args) {
        int stock = 42;
        double price = 69.96;
        char aisle = 'N';
        String product = "Mango";

        System.out.println("Hello! Today we are selling fresh " + product + " at the low price of " + price + " at aisle " + aisle + "! There are only " + stock + " available, so buy now!"  );
    }
}
